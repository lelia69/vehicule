package co.simplon.vehicule.factory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import co.simplon.vehicule.entity.Auto;
import co.simplon.vehicule.entity.Bike;
import co.simplon.vehicule.entity.moto;
import co.simplon.vehicule.entity.vehicule;
import co.simplon.vehicule.repository.IautoRepo;
import co.simplon.vehicule.repository.IbikeRepo;
import co.simplon.vehicule.repository.ImotoRepo;
import co.simplon.vehicule.repository.IvehiculeRepo;

@Component
public class VehiculeFactory {

    @Autowired
    IvehiculeRepo vehiculeRepo;

    @Autowired
    ImotoRepo motoRepo;

    @Autowired
    IbikeRepo bikeRepo;

    @Autowired
    IautoRepo autoRepo;


    public vehicule getVehicule(int id, String type) {
        switch (type) {
            case "bike":
                return (Bike) bikeRepo.findById(id).get();

            case "auto":
                return (Auto) autoRepo.findById(id).get();
            case "moto":
                return (moto) motoRepo.findById(id).get();
            default:
                return null;
        }
    }
}
