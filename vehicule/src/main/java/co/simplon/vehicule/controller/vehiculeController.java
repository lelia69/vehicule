package co.simplon.vehicule.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import co.simplon.vehicule.entity.vehicule;
import co.simplon.vehicule.factory.VehiculeFactory;
import co.simplon.vehicule.repository.IvehiculeRepo;

@RestController
@RequestMapping("/vehicule")
public class vehiculeController {

    @Autowired
    private IvehiculeRepo repo;

    @Autowired
    private VehiculeFactory factoRepo;

    @GetMapping
    public List<vehicule> all() {
        return repo.findAll();
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public vehicule add(@RequestBody @Validated vehicule vehicule) {
        repo.save(vehicule);
        return vehicule;
    }


    @GetMapping("/{id}/{type}")
    public vehicule one(@PathVariable int id, @PathVariable String type) {
        return factoRepo.getVehicule(id, type);
    }



}
