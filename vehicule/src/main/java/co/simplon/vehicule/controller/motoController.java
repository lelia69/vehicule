package co.simplon.vehicule.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import co.simplon.vehicule.entity.moto;
import co.simplon.vehicule.repository.ImotoRepo;

@RestController
@RequestMapping("/moto")
public class motoController {

    @Autowired
    private ImotoRepo repo;

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public moto add(@RequestBody @Validated moto moto) {
        repo.save(moto);
        return moto;
    }


}
