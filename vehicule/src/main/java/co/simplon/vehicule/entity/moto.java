package co.simplon.vehicule.entity;

import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;


@Entity
public class moto extends motorized {

    @Column(columnDefinition = "SMALLINT(4) UNSIGNED")
    private int cylinder;

    @Column(columnDefinition = "SMALLINT(4) UNSIGNED")
    private int horse_power;

    public enum motoType {
        CAFERACER, ROADSTER, TRAIL, MOTOCROSS;
    }

    @Column(columnDefinition = "ENUM('CAFERACER', 'ROADSTER', 'TRAIL', 'MOTOCROSS')")
    @Enumerated(EnumType.STRING)
    private motoType type;

    public moto(){
        
    }

    public moto(int mileage, LocalDate release, int cylinder, int horse_power, motoType type) {
        super(mileage, release);
        this.cylinder = cylinder;
        this.horse_power = horse_power;
        this.type = type;
    }

    public moto(int cylinder, int horse_power, motoType type) {
        this.cylinder = cylinder;
        this.horse_power = horse_power;
        this.type = type;
    }

    public int getCylinder() {
        return cylinder;
    }

    public void setCylinder(int cylinder) {
        this.cylinder = cylinder;
    }

    public int getHorse_power() {
        return horse_power;
    }

    public void setHorse_power(int horse_power) {
        this.horse_power = horse_power;
    }

    

    public void setType(motoType type) {
        this.type = type;
    }

    public motoType getType() {
        return type;
    }



}
