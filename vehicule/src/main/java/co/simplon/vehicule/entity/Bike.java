package co.simplon.vehicule.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;


@Entity
public class Bike extends vehicule {

    @Column(columnDefinition = "ENUM('VTT','BMX','ROAD')")
    @Enumerated(EnumType.STRING)
    private bikeType type;

    public enum bikeType {
        VTT, BMX, ROAD
    }

    @Column(columnDefinition = "SMALLINT(4) UNSIGNED")
    private int bike_trays;

    @Column(columnDefinition = "BOOLEAN DEFAULT FALSE")
    private boolean lock_marking;


    public Bike() {

    }

    public Bike(bikeType type, int bike_trays, boolean lock_marking) {
        this.type = type;
        this.bike_trays = bike_trays;
        this.lock_marking = lock_marking;
    }

    

    public void setType(bikeType type) {
        this.type = type;
    }

    public int getBike_trays() {
        return bike_trays;
    }

    public void setBike_trays(int bike_trays) {
        this.bike_trays = bike_trays;
    }

    public boolean isLock_marking() {
        return lock_marking;
    }

    public void setLock_marking(boolean lock_marking) {
        this.lock_marking = lock_marking;
    }

    public bikeType getType() {
        return type;
    }
}
