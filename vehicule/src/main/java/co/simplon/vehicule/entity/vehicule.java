package co.simplon.vehicule.entity;

import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;



@Entity
@Inheritance(strategy = InheritanceType.JOINED)

public class vehicule {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;

    @Column(length = 50)
    private String brand;

    @Column(length = 50)
    private String color;

    @Column(columnDefinition="FLOAT ", nullable = false)// j'ai voulu mettre unsigned mais ça ne fonctionne pas !!
    private Float price;

    @Column(columnDefinition="DATE", nullable = false)
    private Date date_of_purchase;

    public enum vehiculeType {
        BIKE,MOTO,AUTO;
    }

    @Column(columnDefinition = "ENUM('BIKE','MOTO','AUTO')")
    @Enumerated(EnumType.STRING)
    private vehiculeType vehiculeType;



    public vehicule(){

    }



    public vehicule(int id, String brand, String color, Float price, Date date_of_purchase,
            vehiculeType vehiculeType) {
        this.id = id;
        this.brand = brand;
        this.color = color;
        this.price = price;
        this.date_of_purchase = date_of_purchase;
        this.vehiculeType = vehiculeType;
    }




  



    public int getId() {
        return id;
    }




    public void setId(int id) {
        this.id = id;
    }




    public String getBrand() {
        return brand;
    }




    public void setBrand(String brand) {
        this.brand = brand;
    }




    public String getColor() {
        return color;
    }




    public void setColor(String color) {
        this.color = color;
    }




    public Float getPrice() {
        return price;
    }




    public void setPrice(Float price) {
        this.price = price;
    }




    public Date getDate_of_purchase() {
        return date_of_purchase;
    }




    public void setDate_of_purchase(Date date_of_purchase) {
        this.date_of_purchase = date_of_purchase;
    }



    public vehiculeType getVehiculeType(){
        return vehiculeType;
    }

    public void setVehiculeType(vehiculeType vehiculeType){
        this.vehiculeType = vehiculeType;
    }
    



   



   
}
