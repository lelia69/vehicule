package co.simplon.vehicule.entity;

import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@OnDelete(action = OnDeleteAction.CASCADE)
@Inheritance(strategy = InheritanceType.JOINED)
public class motorized extends vehicule {

    @Column(nullable = false)
    private int mileage;

    @Column(nullable = false)
    private LocalDate release_date;

    public motorized(){

    }

    public motorized(int mileage, LocalDate release_date) {
        this.mileage = mileage;
        this.release_date = release_date;
    }

   

    public int getMileage() {
        return mileage;
    }

    public void setMileage(int mileage) {
        this.mileage = mileage;
    }

    public LocalDate getRelease() {
        return release_date;
    }

    public void setRelease(LocalDate release_date) {
        this.release_date = release_date;
    }

}
