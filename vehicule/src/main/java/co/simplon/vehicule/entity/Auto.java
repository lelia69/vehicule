package co.simplon.vehicule.entity;

import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Entity
public class Auto extends motorized {

    @Column(columnDefinition = "SMALLINT(4) UNSIGNED", nullable = false)
    private int doors_capacity;

    @Column(columnDefinition = "BOOLEAN DEFAULT FALSE")
    private boolean manuel;

    @Column(columnDefinition = "SMALLINT(4) UNSIGNED")
    private int horse_power;

    public enum autoType {

        SUV, BREAK, CROSSOVER
    }

    @Column(columnDefinition = "ENUM('SUV','BREAK','CROSSOVER')")
    @Enumerated(EnumType.STRING)
    private autoType type;

    public Auto(){
        
    }

    public Auto(int mileage, LocalDate release, int doors_capacity, boolean manuel, int horse_power,
            autoType type) {
        super(mileage, release);
        this.doors_capacity = doors_capacity;
        this.manuel = manuel;
        this.horse_power = horse_power;
        this.type = type;
    }

    public Auto(int doors_capacity, boolean manuel, int horse_power, autoType type) {
        this.doors_capacity = doors_capacity;
        this.manuel = manuel;
        this.horse_power = horse_power;
        this.type = type;
    }

    public int getDoors_capacity() {
        return doors_capacity;
    }

    public void setDoors_capacity(int doors_capacity) {
        this.doors_capacity = doors_capacity;
    }

    public boolean isManuel() {
        return manuel;
    }

    public void setManuel(boolean manuel) {
        this.manuel = manuel;
    }

    public int getHorse_power() {
        return horse_power;
    }

    public void setHorse_power(int horse_power) {
        this.horse_power = horse_power;
    }


    public void setType(autoType type) {
        this.type = type;
    }

    public autoType getType() {
        return type;
    }

}
