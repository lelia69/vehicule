package co.simplon.vehicule.entity;

import java.sql.Date;

public class DTO {

    private int id;
    private String brand;
    private String color;
    private Date date_of_purchase;

    public enum vehiculeType {
        BIKE, MOTO, AUTO;
    }

    private vehiculeType vehiculeType;
    private int doors_capacity;
    private boolean manuel;
    private int horse_power;

    public enum autoType {

        SUV, BREAK, CROSSOVER
    }

    private autoType type;
    private int bike_trays;
    private boolean lock_marking;
    private int cylinder;



}
