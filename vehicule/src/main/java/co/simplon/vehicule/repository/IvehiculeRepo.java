package co.simplon.vehicule.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import co.simplon.vehicule.entity.vehicule;

public interface IvehiculeRepo extends JpaRepository<vehicule,Integer> {
    
}
