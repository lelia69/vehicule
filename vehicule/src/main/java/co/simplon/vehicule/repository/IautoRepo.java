package co.simplon.vehicule.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import co.simplon.vehicule.entity.Auto;

public interface IautoRepo extends JpaRepository<Auto,Integer> {
    
}
