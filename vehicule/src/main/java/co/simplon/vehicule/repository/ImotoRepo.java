package co.simplon.vehicule.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import co.simplon.vehicule.entity.moto;

@Repository
public interface ImotoRepo extends JpaRepository<moto,Integer> {
    
}
