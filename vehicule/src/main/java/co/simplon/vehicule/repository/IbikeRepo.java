package co.simplon.vehicule.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import co.simplon.vehicule.entity.Bike;

public interface IbikeRepo extends JpaRepository<Bike,Integer> {
    
}
